package cz.vse.grytsak;
import java.util.*;

/**
 * Třída představuje příkaz snez
 * 
 * @author Leon Grytsak
 * @version 1.0
 */
public class CommandEat implements ICommand
{
    private static final String NAME = "snez";
    GamePlan plan =  new GamePlan();
    private double modifierHealth = 0;
    private double modifierSpeed = 0;
    private int min = 0;
    private int max = 0;
    private static final String SPEED_TEXT = " rychlosti.";
    private static final String HEALTH_TEXT = " životů a ";
    private static final String STRING_TEXT = " jednotek\n a momentálně mám ";

    /**
     * Konstruktor třídy
     * 
     * @param plan přijímá aktuální stav hry z GamePlan a ukládá jej do proměnné plan
     */
    public CommandEat(GamePlan plan)
    {
        this.plan = plan;
    }

    /**
     * Metoda slouží k vyhodnocení situace kdy má postava sníst jídlo
     * Pokud hráč zadá špatný parametr, metoda vrátí textový řetězec o tom že hřáč zadal špatný parametr
     * Pokud hráč zadá více parametrů, metoda vrátí textový řetězec o tom že metoda neumí zpracovat více parametrů
     * Pokud hráč zadá správný parametr metoda zkontroluje zda se jídlo nachazí v inventáří. 
     * Pokud se jídlo nachází v inventáři, podle jeho názvů vygeneruje co se stane dál.
     * 
     * @param parameters název jídla které chceme sníst
     * @return výsledek metody, informace které se vypíší na konzoli
     */
    @Override
    public String process(String... parameters)
    {
        if (parameters.length == 0) {
            return "Orsin: Nevím co mám sníst, musíš zadat název předmětu.";
        }

        if (parameters.length > 1) {
            return "Orsin: Tomu nerozumím, neumím sníst více předmětů najednou.";
        }

        String itemName = parameters[0];

        if(!plan.getInventory().containsItem(itemName)) {        
            return "Předmět " + itemName + " nemáš v inventáři a nebo nejde sníst.";  
        }

        if(plan.getInventory().containsItem(itemName)) {        
            plan.getInventory().deleteItem(itemName);
            switch (itemName) {               
                case "boruvky":
                min = 0;
                max = 10;
                modifierHealth = rngNum(min,max);
                plan.getAtributes().setHealth(modifierHealth);
                plan.getAtributes().setSpeed();
                return "Orsin: snědl jsem borůvky. Zdraví se změnilo o " + modifierHealth + STRING_TEXT + plan.getAtributes().getHealth() + HEALTH_TEXT + plan.getAtributes().getSpeed() + SPEED_TEXT;

                case "jedla_trava":
                min = -15;
                max = 5;
                modifierHealth = rngNum(min,max);
                min = 2;
                max = 5;
                modifierSpeed = rngNum(min,max);
                plan.getAtributes().setHealth(modifierHealth);
                plan.getAtributes().setSpeedItems(modifierSpeed);
                plan.getAtributes().setSpeed();
                return "Orsin: snědl jsem jedlou trávu. Zdraví se změnilo o " + modifierHealth +" jednotek. Rychlost se změnila o " + modifierSpeed + STRING_TEXT + plan.getAtributes().getHealth() + HEALTH_TEXT + plan.getAtributes().getSpeed() + SPEED_TEXT;

                case "zive_kvetiny":
                min = 10;
                max = 20;
                modifierHealth = rngNum(min,max);
                min = -10;
                max = 0;
                modifierSpeed = rngNum(min,max);
                plan.getAtributes().setHealth(modifierHealth);
                plan.getAtributes().setSpeedItems(modifierSpeed);
                plan.getAtributes().setSpeed();
                return "Orsin: snědl jsem živé květiny. Zdráví se změnilo o " + modifierHealth + " jednotek. Rychlost se změnila o " + modifierSpeed + STRING_TEXT + plan.getAtributes().getHealth() + HEALTH_TEXT + plan.getAtributes().getSpeed() + SPEED_TEXT;
                case "brouky":
                min = 1;
                max = 12;
                modifierHealth = rngNum(min,max);
                min = -3;
                max = 5;
                modifierSpeed = rngNum(min,max);
                plan.getAtributes().setHealth(modifierHealth);
                plan.getAtributes().setSpeedItems(modifierSpeed);
                plan.getAtributes().setSpeed();
                return "Orsin: snědl jsem brouky. Rychlost se změnila o: " + modifierSpeed + " jednotek a zdraví o: " + modifierHealth + STRING_TEXT + plan.getAtributes().getHealth() + HEALTH_TEXT + plan.getAtributes().getSpeed() + SPEED_TEXT;
                case "vejce":
                min = -20;
                max = 20;
                modifierHealth = rngNum(min,max);
                min = -20;
                max = 5;
                modifierSpeed = rngNum(min,max);
                plan.getAtributes().setHealth(modifierHealth);
                plan.getAtributes().setSpeedItems(modifierSpeed);
                plan.getAtributes().setSpeed();
                return "Orsin: snědl jsem vejce. Rychlost se změnila o: " + modifierSpeed + " jednotek a zdraví o: " + modifierHealth + STRING_TEXT + plan.getAtributes().getHealth() + HEALTH_TEXT + plan.getAtributes().getSpeed() + SPEED_TEXT;
            }
        }

        return "Tento předmět nejde sníst, ale použít.";
    }

    /**
     * Metoda vrací název příkazu
     * 
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return NAME;
    }

    /**
     * Metoda slouží k generování náhodného čísla
     * 
     * @param min, max určují rozsah pro generování náhodného čísla
     * @return náhodně vygenerované číslo
     */
    public int rngNum(int min, int max)
    {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }
}