package cz.vse.grytsak;

/**
 * Třída slouží k vypsání údajů o lokaci
 * 
 * @author Leon Grytsak
 * @version 1.0
 */
public class CommandLocation implements ICommand
{
    private static final String NAME = "lokace"; 
    private GamePlan plan;
    
    /**
     * Konstruktor třídy
     * 
     * @param plan přijímá aktuální stav hry z GamePlan a ukládá jej do proměnné plan
     */
    public CommandLocation(GamePlan plan)
    {
        this.plan = plan;    
    }

    /**
     * Metoda slouží k vypsání údajů o lokaci pomocí metody v třídě Area
     * 
     * @param parameters přijímá ale nevyužívá
     * @return výsledek metody
     */
    @Override
    public String process(String... parameters)
    {
        if (parameters.length > 0) {
            return "K tomuto příkazu nezadávejte parametry.";
        }
        
        return plan.getCurrentArea().getFullDescription();
    }
    
    /**
     * Metoda vrací název příkazu
     * 
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return NAME;
    }   
}