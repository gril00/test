package cz.vse.grytsak;

/**
 * Třída slouží pro výpis předmětů v inventáři
 * 
 * @author Leon Grytsak
 * @version 1.0
 */
public class CommandInventory implements ICommand
{
    private static final String NAME = "inventar"; 
    private GamePlan plan;

    /**
     * Konstruktor třídy
     * 
     * @param plan - příjímá aktualní stav hry z GamePlan do promenné plan
     */
    public CommandInventory(GamePlan plan)
    {
        this.plan = plan;    
    }

    /**
     * Metoda se zavolá po zadání příkazu 'inventar' a zavolá metody v inventáři pomocí kterých zkontroluje
     * zda se v inventáři něco nachází. Pokud ano, vypíše obsah inventáře pomocí metody v třídě Inventory
     * 
     * @param parameters parametry přijmou může ale nevyuživá je
     * @return výsledkem výpisu inventáře (buď v něm nic není a nebo vypíše co v něm je)
     */
    @Override
    public String process(String... parameters)
    {
        if (parameters.length > 0) {
            return "K tomuto příkazu nezadávejte parametry.";
        }
        
        if(plan.getInventory().getItemsList().isEmpty()) {       
            return "Orsin: Zatím jsem nic nesebral a nebo jsem předměty položil.";    
        }
        else {        
            return "Orsin: V inventáři mám " + plan.getInventory().getItemsList() +".";
        }

    }

    /**
     * Metoda vrací název příkazu
     * 
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return NAME;
    }   
}