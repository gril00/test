package cz.vse.grytsak;
import java.util.Random;

/**
 * Třída představující aktuální stav hry. Veškeré informace o stavu hry
 * <i>(mapa lokací, inventář, vlastnosti hlavní postavy, informace o plnění
 * úkolů apod.)</i> by měly být uložené zde v podobě datových atributů.
 * <p>
 * Třída existuje především pro usnadnění potenciální implementace ukládání
 * a načítání hry. Pro uložení rozehrané hry do souboru by mělo stačit uložit
 * údaje z objektu této třídy <i>(např. pomocí serializace objektu)</i>. Pro
 * načtení uložené hry ze souboru by mělo stačit vytvořit objekt této třídy
 * a vhodným způsobem ho předat instanci třídy {@link Game}.
 *
 * @author Michael Kölling
 * @author Luboš Pavlíček
 * @author Jarmila Pavlíčková
 * @author Jan Říha
 * @author Leon Grytsak
 * @version 1.0
 *
 * @see <a href="https://java.vse.cz/4it101/AdvSoubory">Postup pro implementaci ukládání a načítání hry na předmětové wiki</a>
 * @see java.io.Serializable
 */
public class GamePlan
{
    private static final String FINAL_LOCATION_NAME = "stara_budova";
    private static final String KEY_PART_TEXT = "Část klíče kterou potřebuju k sestavení klíče na otevření budovy s portálem.";

    private Area currentArea;
    private Inventory inventory;
    private KeyInventory invK;
    private Atributes atributes;
    private Monster monster;

    private Item kovarna;

    private Item boruvka;
    private Item jedla_trava;
    private Item ziva_kvetina;
    private Item brouk;
    private Item vejce;

    private Item obvazy;
    private Item medkit;

    private Item keypart1;
    private Item keypart2;
    private Item keypart3;
    private Item keypart4;

    private Area vchodDoLesa;
    private Area baziny;
    private Area jeskyne;
    private Area jeskyneHlubiny;
    private Area reka;
    private Area staraKovarna;
    private Area srdceLesa;
    private Area staraBudova;

    /**
     * Konstruktor třídy. Pomocí metody {@link #prepareWorldMap() prepareWorldMap}
     * vytvoří jednotlivé lokace a propojí je pomocí východů.
     */
    public GamePlan()
    {
        prepareWorldMap();
        inventory = new Inventory();
        invK = new KeyInventory();
        atributes = new Atributes();
        monster = new Monster(this);
    }

    /**
     * Metoda vytváří jednotlivé lokace a propojuje je pomocí východů. Jako
     * výchozí aktuální lokaci následně nastaví domeček, ve kterém bydlí
     * Karkulka.
     */
    private void prepareWorldMap()
    {
        // Vytvoříme jednotlivé lokace
        vchodDoLesa = new Area("vchod_do_lesa","Vchod do aeterického lesa před pevností.");
        baziny = new Area("baziny", "Jsi v bažinách. V dáli vidíš vchod do jeskyně a nebo předchod k řece.");
        jeskyne = new Area("jeskyne","Žeby se jednalo o doupě příšery? A hele! Támhle pokračuje.");
        jeskyneHlubiny = new Area("jeskyne_hlubiny","Hlubiny jeskyně. Měl bych být opatrný.");
        reka = new Area("reka","Vede sem hodně cest.");
        staraKovarna = new Area("stara_kovarna","Stará kovárna. Tady bych možná mohl opravit klíč.");
        srdceLesa = new Area("srdce_lesa","Srdce aeterického lesa. Skener zaznamenal velké množství energie.");
        staraBudova = new Area(FINAL_LOCATION_NAME,"Stara budova. Tady je můj portál!");

        // Nastavíme průchody mezi lokacemi (sousední lokace)
        vchodDoLesa.addExit(baziny);
        baziny.addExit(vchodDoLesa);

        baziny.addExit(jeskyne);
        jeskyne.addExit(baziny);

        jeskyne.addExit(jeskyneHlubiny);
        jeskyneHlubiny.addExit(jeskyne);

        jeskyne.addExit(reka);
        reka.addExit(jeskyne);

        reka.addExit(srdceLesa);
        srdceLesa.addExit(reka);

        reka.addExit(staraKovarna);
        staraKovarna.addExit(reka);

        srdceLesa.addExit(staraBudova);
        staraBudova.addExit(srdceLesa);

        // Přidáme předměty do lokací

        kovarna = new Item("kovarna","Tady můžu opravit klíč!",true, true);
        staraKovarna.addItem(kovarna);

        boruvka = new Item("boruvky", "Mimozemská borůvka", true, false);
        rngAddItemToArea(boruvka);

        jedla_trava = new Item("jedla_trava", "Podivná jedlá tráva.", true, false);
        rngAddItemToArea(jedla_trava);

        ziva_kvetina = new Item("zive_kvetiny", "Hýbající se květina.", true, false);
        rngAddItemToArea(ziva_kvetina);

        brouk = new Item("brouky", "Mimozemský brouk. Vypadá jedle.", true, false);
        rngAddItemToArea(brouk);

        vejce = new Item("vejce", "Mimozemské vejce. Řekl bych že je zkažené.", true, false);
        rngAddItemToArea(vejce);

        obvazy = new Item("obvazy", "Obvazy. Ty se určitě hodí!", true, false);
        rngAddItemToArea(obvazy);

        medkit = new Item("lekarnicka", "Lekarnička. Použít v největší nouzi!", true,false);
        rngAddItemToArea(medkit);

        keypart1 = new Item("prvni_cast_klice", KEY_PART_TEXT, false, false);
        rngAddItemToArea(keypart1);

        keypart2 = new Item("druha_cast_klice", KEY_PART_TEXT, false, false);
        rngAddItemToArea(keypart2);

        keypart3 = new Item("treti_cast_klice", KEY_PART_TEXT, false, false);
        rngAddItemToArea(keypart3);

        keypart4 = new Item("ctvrta_cast_klice", KEY_PART_TEXT, false, false);
        rngAddItemToArea(keypart4);

        // Hru začneme v domečku
        currentArea = vchodDoLesa;
    }

    /**
     * Metoda slouží pro náhodne generování čísla
     * 
     * @param maxAreas představuje maximální počet oblastí a také maximální možné vygenerované číslo
     * @return náhodně vygenerované číslo
     */
    private int rngNumber(int maxAreas)
    {
        Random rng = new Random();

        int rngNum = rng.nextInt(maxAreas);

        return rngNum;
    }

    /**
     * Metoda slouží k přidání itemů do lokace
     * 
     * @param item předmět který se má přidat do lokace (není String ale Item)
     */
    public void rngAddItemToArea(Item item)
    {
        int maxAreas = 6; //počet místností ve kterých se můžou spawnout itemy
        int rngMove = 0;

        rngMove = rngNumber(maxAreas);
        
        if(rngMove == 0) {
            baziny.addItem(item);
        }
        if(rngMove == 1) {
            jeskyne.addItem(item);
        }
        if(rngMove == 2) {
            jeskyneHlubiny.addItem(item);
        }
        if(rngMove == 3) {
            reka.addItem(item);
        }
        if(rngMove == 4) {
            staraKovarna.addItem(item);
        }
        if(rngMove == 5) {
            srdceLesa.addItem(item);
        }
        if(rngMove == 6) {
            staraBudova.addItem(item);
        }
    }

    /**
     * Metoda vrací odkaz na aktuální lokaci, ve které se hráč právě nachází.
     *
     * @return aktuální lokace
     */
    public Area getCurrentArea()
    {
        return currentArea;
    }

    /**
     * Metoda nastaví aktuální lokaci, používá ji příkaz {@link CommandMove}
     * při přechodu mezi lokacemi.
     *
     * @param area lokace, která bude nastavena jako aktuální
     */
    public void setCurrentArea(Area area)
    {
        currentArea = area;
    }

    public boolean isVictorious()
    {
        atributes.setHealth(0);        
        if (FINAL_LOCATION_NAME.equals(currentArea.getName()) && invK.repairedKey() && atributes.getHealth() > 0)
        {
            return FINAL_LOCATION_NAME.equals(currentArea.getName());
        }        
        return false;
    }   

    public boolean isDefeated()
    {
        if(getAtributes().getHealth() == 0){        
            return true;  
        }
        return false;
    }

    public Inventory getInventory()
    {
        return this.inventory;
    }

    public KeyInventory getKeyInventory()
    {
        return this.invK;
    }

    public Atributes getAtributes()
    {
        return this.atributes;
    }

    public Area getSmeltery()
    {
        return staraKovarna;
    }

    public Monster getMonster()
    {
        return monster;
    }
}
