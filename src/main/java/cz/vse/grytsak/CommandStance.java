package cz.vse.grytsak;

/**
 * Třída slouží k změne postoje herní postavy
 * 
 * @author Leon Grytsak
 * @version 1.0
 */
public class CommandStance implements ICommand
{
    private static final String NAME = "postoj"; 
    private GamePlan plan;

    /**
     * Konstruktor třídy
     * 
     * @param plan přijímá aktuální stav hry z GamePlan a ukládá jej do proměnné plan
     */
    public CommandStance(GamePlan plan)
    {
        this.plan = plan;    
    }    
    
    /**
     * Metoda nastaví postoj pokud zadáte správně parametr
     * 
     * @param parameters název postoje (normalni/skryty)
     * @return výsledek změny postoje
     */
    @Override
    public String process(String... parameters)
    {
        if (parameters.length == 0) {
            return "Orsin: Nevím jaký postoj mám zaujmout.";
        }

        if (parameters.length > 1) {
            return "Orsin: Tomu nerozumím, neumím zaujmout oba postoje zároveň.";
        }

        String stanceName = parameters[0];

        switch (stanceName) {        
            case "skryty":
            plan.getAtributes().setStance(true);
            return "Zaujal jsi postoj 'skrytý'.";
            case "normalni":
            plan.getAtributes().setStance(false);
            return "Zaujal jsi 'normální' postoj.";
        }

        return "Chyba, zkus použít příkaz znova.";
    }
    
    /**
     * Metoda vrací název příkazu
     * 
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return NAME;
    }
}