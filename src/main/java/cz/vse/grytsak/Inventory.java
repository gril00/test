package cz.vse.grytsak;
import java.util.List;
import java.util.ArrayList;

/**
 * Třída představuje inventář kam si postava ukládá předměty
 * 
 * @author Leon Grytsak
 * @version 1.0
 */
public class Inventory 
{   
    private List<Item> inv;
    private static final int MAX_ITEMS = 3;

    /**
     * Konstruktor třídy - nadeklaruje nové pole ArrayList které bude sloužit k ukládání itemů, používá Item
     */    
    public Inventory() 
    {
        inv = new ArrayList<Item>();
    }

    /**
     * Metoda přidá item pokud inventář není plný
     * 
     * @param addedItem představuje předmět který chceme přidat
     * @return null pokud se item nepřidal
     */ 
    public Item itemAdd(Item addedItem)
    {
        if(notFull()) {
            inv.add(addedItem);
            return addedItem;
        }
        return null;
    }

    /**
     * Metoda zjištuje zda je inventář plný či nokoliv pomocí nadeklarované statické proměnné MAX_ITEMS
     * 
     * @return inventář je plný (false), není plný (true)

     */
    public boolean notFull()
    {
        if(inv.size() < MAX_ITEMS) {
            return true;
        }
        return false;
    }

    /**
     * Metoda zjištuje zda je v inventáři hledaný item, hledá se podle Stringového názvu a pomocí cyklu for each
     * 
     * @param searched předmět který hledáme
     * @return předmět se v inventáři nachází (true) a nebo nenachází (false)
     */
    public boolean containsItem (String searched) 
    {
        for (Item current : inv) {
            if(current.getName().equals(searched)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Metoda vypíše všechny předměty v inventáři
     * 
     * @return seznam předmětů
     */
    public String getItemsList() 
    {
        String list = "";
        for (Item current : inv) {
            if(!list.equals("")){
                list += ", ";
            }
            list += " " + current.getName();
        }
        return list;
    }

    /**
     * Metoda slouží k odstranění předmětu z inventáře
     * 
     * @param undesirable item který chceme odstranit
     * @return odstraněný předmět
     */
    public Item deleteItem (String undesirable)
    {
        Item deleted = null;
        for (Item current: inv) {
            if(current.getName().equals(undesirable)) {
                deleted = current;
                inv.remove(current);
                break;
            }
        }
        return deleted;
    }

    /**
     * Metoda slouží k zjištění názvu proměnné daného předmětu podle jména
     * 
     * @param name název předmětu ve stringu
     * @return název předmětu v Item
     */
    public Item getItem(String name)
    {
        Item searched = null;
        for (Item current : inv) {

            if(current.getName().equals(name)) {

                searched = current;
                break;
            }
        }
        return searched;
    }

}